#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transform.h"
#include "util.h"
#include <stdio.h>

static const char *in_msg[] = {
    [OPEN_ERROR] = "Could not open the file for reading"
};
static const char *out_msg[] = {
    [OPEN_ERROR] = "Could not create a file for writing"
};
static const char *r_msg[] = {
    [READ_INVALID_SIGNATURE] ="Invalid bitmap signature",
    [READ_INVALID_BITS] = "Invalid number of bits",
    [READ_INVALID_HEADER] = "Invalid bitmap header",
    [READ_ERROR] = "Error while reading"
};
static const char *w_msg[] = {
    [WRITE_ERROR] = "Error while writing"
};

void usage(const char *filename) {
  fprintf(stderr,"Usage: %s <source-image> <transformed-image>\n", filename);
}

int main(int argc, char *argv[]) {
  if (argc != 3) usage(argv[0]);
  if (argc < 3) err("Not enough arguments\n");
  if (argc > 3) err("Too many arguments\n");

  FILE *in, *out;
  enum open_status stat;
  if ((stat = open_file(&in, argv[1], "rb")) != 0) {
    err("%s\n", in_msg[stat]);
  }
  if ((stat = open_file(&out, argv[2], "wb")) != 0) {
    close_file(&in);
    err("%s\n", out_msg[stat]);
  }

  struct image source = {0};

  enum read_status read_stat = from_bmp(in, &source);
  close_file(&in);
  if (read_stat != READ_OK) {
    err("%s\n", r_msg[read_stat]);
  }

  struct maybe_image image = rotate_left(source);
  struct image transformed = image.value;
  if (!image.valid) {
    image_destroy(&source);
    image_destroy(&transformed);
    err("Could not rotate the image!\n");
  }

  enum write_status write_stat = to_bmp(out, &transformed);
  close_file(&out);
  if (write_stat != WRITE_OK) {
    image_destroy(&source);
    image_destroy(&transformed);
    err("%s\n", w_msg[write_stat]);
  }

  image_destroy(&source);
  image_destroy(&transformed);

  return 0;
}
