#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct maybe_image rotate_left(struct image);

#endif
