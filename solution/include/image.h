#ifndef IMAGE_H
#define IMAGE_H

#include "dimensions.h"
#include "maybe.h"

struct pixel { uint8_t r, g, b; };

struct image {
  struct dimensions dims;
  struct pixel *data;
};

DECLARE_MAYBE_HEADER(image, struct image)

struct maybe_image image_create(struct dimensions);
void image_destroy(struct image *);

#endif
